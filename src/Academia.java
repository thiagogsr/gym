import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import exception.ExercicioJaCadastradoException;
import exception.PessoaJaCadastradaException;

public class Academia {
	
	private String nome;
	private Set<Pessoa> listaPessoas;
	private Map<Integer, Exercicio> listaExercicios;
	
	public Academia(String nome) {
		this.nome = nome;
		this.listaPessoas = new HashSet<Pessoa>();
		this.listaExercicios = new HashMap<Integer, Exercicio>();
	}
	
	public void ContratarInstrutor(Instrutor instrutor) throws PessoaJaCadastradaException {
		if (this.listaPessoas.contains(instrutor))
			throw new PessoaJaCadastradaException();
		
		this.listaPessoas.add(instrutor);
	}
	
	public void MatricularAluno(Aluno aluno) throws PessoaJaCadastradaException {
		if (this.listaPessoas.contains(aluno))
			throw new PessoaJaCadastradaException();
		
		this.listaPessoas.add(aluno);
	}
	
	public void MatricularAluno(Aluno aluno, int qtdParcelas) throws PessoaJaCadastradaException {
		if (this.listaPessoas.contains(aluno))
			throw new PessoaJaCadastradaException();
		
		int valor = 100;
		double valorComDesconto = valor - (valor * aluno.PerDesconto());
		for (int i = 1; i <= qtdParcelas; i++) {
			aluno.CadastrarMensalidades(i, valorComDesconto);
		}
		
		this.listaPessoas.add(aluno);
	}
	
	public void CadastrarExercicio(Exercicio exercicio) {
		this.listaExercicios.put(exercicio.getCodigo(), exercicio);
	}
	
	public void AdicionarFichaAluno(int codExercicio, int codAluno) throws ExercicioJaCadastradoException {
		Exercicio exercicio = this.listaExercicios.get(codExercicio);
		Aluno aluno = null;
		
		for (Pessoa a: this.listaPessoas) {
			if (a instanceof Aluno && a.getCodigo() == codAluno) {
				aluno = (Aluno) a;
				break;
			}
		}
		
		aluno.CadastrarExercicio(exercicio);
	}
	
	public int QuantidadeAlunosAtraso() {
		int quantidade = 0;
		
		for (Pessoa p: this.listaPessoas) {
			if (p instanceof Aluno) {
				for (Mensalidade m: ((Aluno) p).getListaMensalidades()) {
					if (m.getStatus() == 'A') {
						quantidade++;
						break;
					}
				}
			}
		}
		
		return quantidade;
	}
	
	public void InformarAvaliacao(int codAluno, int codigo, float peso, float altura) throws Exception {
		Aluno aluno = null;
		
		for (Pessoa p: this.listaPessoas) {
			if (p instanceof Aluno && p.getCodigo() == codAluno)
				aluno = (Aluno) p;
		}
		
		if (aluno == null)
			throw new Exception("N�o foi poss�vel realizar a avalia��o");
		
		aluno.CadastrarAvaliacao(codigo, peso, altura);
	}
	
	@Override
	public String toString() {
		return this.nome;
	}

}

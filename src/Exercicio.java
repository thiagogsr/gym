public class Exercicio {
	
	private int codigo;
	private String descricao;
	private short qtdRepeticoes;
	private int carga;
	private short diaSemana;
	
	public Exercicio(int codigo, String descricao, short qtdRepeticoes, int carga, short diaSemana) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.qtdRepeticoes = qtdRepeticoes;
		this.carga = carga;
		this.diaSemana = diaSemana;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}

}

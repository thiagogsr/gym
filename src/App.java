import exception.PessoaJaCadastradaException;


public class App {

	/**
	 * @param args
	 * @throws PessoaJaCadastradaException 
	 */
	public static void main(String[] args) throws PessoaJaCadastradaException {
		Aluno aluno = new Aluno("Thiago", (short) 22, false, "Teste", new Instrutor("Teste", (short) 22, 2012, true, 0));
		Aluno aluno2 = new Aluno("Thiago", (short) 22, false, "Teste", new Instrutor("Teste", (short) 22, 2012, true, 0));
		Aluno aluno3 = new Aluno("Thiago", (short) 22, false, "Teste", new Instrutor("Teste", (short) 22, 2012, true, 0));
		Academia academia = new Academia("Teste");
		
		academia.MatricularAluno(aluno, 10);
		academia.MatricularAluno(aluno2, 5);
		academia.MatricularAluno(aluno3, 2);
		System.out.println(academia.QuantidadeAlunosAtraso());
	}

}

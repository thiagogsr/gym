public abstract class Pessoa {
	
	private int codigo;
	private String nome;
	private short idade;
	private static int seq;
	
	public Pessoa (int codigo, String nome, short idade) {
		this.codigo = codigo;
		this.nome = nome;
		this.idade = idade;
	}
	
	public abstract double PerDesconto();
	
	public int getCodigo() {
		return codigo;
	}

	public short getIdade() {
		return idade;
	}
	
	static int getSeq() {
		seq++;
		return seq;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
	
	@Override
	public int hashCode() {
		return codigo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Pessoa) {
			Pessoa p = (Pessoa) obj;
			return this.codigo == p.codigo;
		}
		return false;
	}

}

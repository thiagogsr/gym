public class Instrutor extends Pessoa {
	
	private int anoFormacao;
	private boolean personal;
	private double salarioBase;
	
	public Instrutor(String nome, short idade, int anoFormacao, boolean personal, double salario) {
		super(getSeq(), nome, idade);
		this.anoFormacao = anoFormacao;
		this.personal = personal;
		this.salarioBase = salario;
	}
	
	@Override
	public double PerDesconto() {
		double valor = 0f;
		
		if (this.personal)
		 valor = .05;
		
		return valor;
	}
	
	public boolean getPersonal() {
		return personal;
	}

}

public class Mensalidade {
	
	private double valor;
	private int nrParcela;
	private char status;
	
	public Mensalidade(double valor, int nrParcela) {
		this.valor = valor;
		this.nrParcela = nrParcela;
		this.status = 'A';
	}

	public char getStatus() {
		return status;
	}

}

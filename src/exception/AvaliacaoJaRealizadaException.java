package exception;

public class AvaliacaoJaRealizadaException extends Exception {

	public AvaliacaoJaRealizadaException() {
		super("J� foi realizada uma avalia��o neste m�s");
	}
	
}

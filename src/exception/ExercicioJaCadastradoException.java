package exception;

public class ExercicioJaCadastradoException extends Exception {
	
	public ExercicioJaCadastradoException(String descricao) {
		super("O exercício " + descricao + " já foi cadastrado");
	}

}

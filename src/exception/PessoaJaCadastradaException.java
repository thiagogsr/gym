package exception;

public class PessoaJaCadastradaException extends Exception {
	
	public PessoaJaCadastradaException() {
		super("A pessoa j� est� cadastrada");
	}

}

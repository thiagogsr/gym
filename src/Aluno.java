import java.util.HashSet;
import java.util.Set;

import exception.AvaliacaoException;
import exception.AvaliacaoJaRealizadaException;
import exception.ExercicioJaCadastradoException;

public class Aluno extends Pessoa {
	
	private boolean problemasSaude;
	private String objetivo;
	private Instrutor profResponsavel;
	private Set<Exercicio> listaExercicios;
	private Set<Avaliacao> listaAvaliacao;
	private Set<Mensalidade> listaMensalidades;

	public Aluno(String nome, short idade, boolean problemasSaude, String objetivo, Instrutor professorResponsavel) {
		super(getSeq(), nome, idade);
		this.problemasSaude = problemasSaude;
		this.objetivo = objetivo;
		this.profResponsavel = professorResponsavel;
		this.listaExercicios = new HashSet<Exercicio>();
		this.listaAvaliacao = new HashSet<Avaliacao>();
		this.listaMensalidades = new HashSet<Mensalidade>();
	}
	
	public void CadastrarExercicio(Exercicio exercicio) throws ExercicioJaCadastradoException {
		if (this.listaExercicios.contains(exercicio))
			throw new ExercicioJaCadastradoException(exercicio.getDescricao());
		
		this.listaExercicios.add(exercicio);
	}
	
	public void CadastrarAvaliacao(int codigo, float peso, float altura) throws AvaliacaoException {
		for (Avaliacao a: this.listaAvaliacao) {
			if (a.getCodigo() == codigo)
				throw new AvaliacaoException();
		}
		
		this.listaAvaliacao.add(new Avaliacao(codigo, peso, altura));
	}
	
	public void RealizarAvaliacao(Avaliacao avaliacao) throws AvaliacaoJaRealizadaException {
		if (this.listaAvaliacao.contains(avaliacao))
			throw new AvaliacaoJaRealizadaException();
		
		this.listaAvaliacao.add(avaliacao);
	}
	
	public void CadastrarMensalidades(int nrParcela, double valor) {
		this.getListaMensalidades().add(new Mensalidade(valor, nrParcela));
	}

	@Override
	public double PerDesconto() {
		double valor = 0f;
		
		if (!this.problemasSaude) {
			if (this.getIdade() > 50 || !this.profResponsavel.getPersonal())
				valor = .05;
		}
		
		return valor;
	}

	public Set<Mensalidade> getListaMensalidades() {
		return listaMensalidades;
	}

}

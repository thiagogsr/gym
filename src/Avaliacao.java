public class Avaliacao {

	private int codigo;
	private float altura;
	private float peso;
	private short mes;
	
	public Avaliacao(int codigo, float peso, float altura) {
		this.codigo = codigo;
		this.peso = peso;
		this.altura = altura;
	}
	
	public Avaliacao(float peso, float altura, short mes) {
		this.peso = peso;
		this.altura = altura;
		this.mes = mes;
	}
	
	public int getCodigo() {
		return codigo;
	}

	public short getMes() {
		return mes;
	}
	
	@Override
	public int hashCode() {
		return mes;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Avaliacao) {
			Avaliacao a = (Avaliacao) obj;
			return this.mes == a.mes;
		}
		return false;
	}

}
